module gitlab.com/gitlab-org/cloudflare_exporter

go 1.14

require (
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d // indirect
	github.com/go-kit/kit v0.10.0
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.2.0 // indirect
	github.com/oklog/run v1.1.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/client_golang v1.6.0
	github.com/prometheus/common v0.10.0
	github.com/stretchr/testify v1.4.0
	golang.org/x/sys v0.0.0-20200515095857-1151b9dac4a9 // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
